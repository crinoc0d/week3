public class Amphibian {
    String[] characteristics = { "Cold blooded", "Can live both in water and land"};

    public void reproduces() {
        System.out.println("Amphibian reproduces");
    }

    public void eats() {
        System.out.println("Amphibian eats");
    }

    public void vocalizes() {
        System.out.println("Amphibian eats");
    }
}
