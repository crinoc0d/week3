public class Frog extends Amphibian {
    String[] characteristics = { "Croaks", "Eats bugs"};

    public void jumps() {
        System.out.println("Frog jumps");
    }

    public static void main(String[] args) {
        Amphibian amphibian = new Frog();
        amphibian.eats();
        amphibian.reproduces();
        amphibian.vocalizes();
        //a.jumps(); //not available in Amphibian class
        ((Frog)amphibian).jumps();
    }
}
