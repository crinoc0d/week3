public class Survey {
    public static void main(String[] args) {
        Person person1 = new Person(new String[] {"Jack", "Jr."} , "Damon");
        person1.nationality = "american";
        person1.setBirthDate("1986/11/15");
        person1.selfDescribe();

        Person person2 = new Person("Michelle Aline Badot");
        person2.nationality = "french";
        person2.setBirthDate("1980/02/25");
        person2.selfDescribe();

        Person person3 = new Person(new String[] {"Maria"} , "Fernandez");
        person3.nationality = "spanish";
        person3.setBirthDate("1991/07/21");
        person3.selfDescribe();
    }
}
