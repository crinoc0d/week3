import java.util.Calendar;

public abstract class PersonDetails {
    protected String surname;
    protected String[] firstname;
    protected String nationality;
    protected Calendar birthdate;

    PersonDetails(String[] firstname, String surname) {
        this.firstname = firstname;
        this.surname = surname;
        if (firstname == null) {
            this.firstname = new String[] {""};
        }
        if (surname == null) {
            this.surname = "";
        }
    }

    PersonDetails(String fullname) {
        String[] names = fullname.split(" ");
        surname = names[names.length - 1];
        firstname = new String[names.length];

        for (int i = 0; i < names.length - 1; i++) {
            firstname[i] = names[i];
        }
    }

    abstract Calendar getBirthDate();
    abstract void selfDescribe();
}
