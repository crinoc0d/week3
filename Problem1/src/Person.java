import java.util.Calendar;
import java.util.Date;
import java.text.SimpleDateFormat;

public class Person extends PersonDetails {
    Person(String[] firstname, String surname) {
        super(firstname, surname);
    }

    Person(String fullname) {
        super(fullname);
    }

    void printName() {
        for (int i = 0; i < firstname.length - 1; i++) {
            System.out.print(firstname[i] + " ");
        }
        System.out.println(surname);
    }

    public Calendar getBirthDate() {
        return birthdate;
    }

    public void setBirthDate(String date) {
        String[] dateFormat = date.split("\\/");
        birthdate = Calendar.getInstance();
        // year/month/day
        birthdate.set(Integer.parseInt(dateFormat[0]),
                Integer.parseInt(dateFormat[1]),
                        Integer.parseInt(dateFormat[2]));
    }

    public void selfDescribe() {
        SimpleDateFormat ft = new SimpleDateFormat ("MMM dd, YYYY");
        Calendar today = Calendar.getInstance();
        int age = today.get(Calendar.YEAR) - getBirthDate().get(Calendar.YEAR);

        today.setTime(new Date());
        if(today.get(Calendar.MONTH) < getBirthDate().get(Calendar.MONTH))
            age--;

        if (nationality.equals("american")) {
            System.out.print("My names is ");
            printName();
            System.out.println("My nationality is american. I am born at " + ft.format(getBirthDate().getTime()) + ". ");
            System.out.println("I am " + age + " years old.");
            System.out.println();
        } else if (nationality.equals("french")) {
            System.out.print("Je m'apelle ");
            printName();
            System.out.println("Ma nationalite est francaise. Je suis ne en " + ft.format(getBirthDate().getTime()) + ". ");
            System.out.println("J'ai " + age + " ans.");
            System.out.println();
        } else if (nationality.equals("spanish")) {
            System.out.print("Mi nombre est ");
            printName();
            System.out.println("Mi nacionalidad es espanola. I soy naci el " + ft.format(getBirthDate().getTime()) + ". ");
            System.out.println("Tengo " + age + " anos.");
            System.out.println();
        }
    }
}