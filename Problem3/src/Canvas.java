public class Canvas extends Panel {
    final int MAX_NO_SHAPES = 5;
    final int idxCanvas = idxCanvases + 1;
    private Shape[] shapes;
    int idxShapes = 0;
    int idxCircles = 0;
    int idxRectangles = 0;

    class Circle implements Shape {
        private double radius;
        final int idxCircle = idxCircles + 1;

        Circle(int r) {

            radius = r;
            System.out.println(this.toString() + " with radius " + radius + " was created");
            idxCircles++;
        }

        public double getRadius() {
            return radius;
        }

        public void printArea() {
            System.out.println("Area of circle with radius " + radius + " is " + 2 * Math.PI * radius);
        }

        @Override
        public String toString() {
            return "Circle " + idxCircle;
        }
    }

    class Rectangle implements Shape {
        private double width;
        private double length;
        private Line[] lines = new Line[4];
        final int idxRectangle = idxRectangles + 1;

        Rectangle(double x0, double y0, double x1, double y1) {
            lines[0] = new Line(new Point(x0, y0), new Point(x0, y1));
            lines[1] = new Line(new Point(x0, y1), new Point(x1, y1));
            lines[2] = new Line(new Point(x1, y1), new Point(x1, y0));
            lines[3] = new Line(new Point(x1, y0), new Point(x0, y0));
            width = lines[0].getLength();
            length = lines[1].getLength();
            System.out.println(this.toString() + " with width " + width + " and length " + length + " was created");
            idxRectangles++;
        }

        public void printArea() {
            System.out.println("Area of rectangle with width " + width + " and length " + length + " is " + width * length);
        }

        public Line[] getLines() {
            return lines;
        }

        @Override
        public String toString() {
            return "Rectangle " + idxRectangle;
        }
    }

    class Triangle implements Shape {
        double base;
        double height;
        Line[] lines = new Line[3];
        Point[] points = new Point[3];

        public void printArea() {
            System.out.println("Area of triangle with base " + base + " and height " + height + " is " + (base * height) / 2);
        }
    }

    Shape getCircle(int r) {
        return new Circle(r);
    }

    Shape getRectangle(double x0, double y0, double x1, double y1) {
        return new Rectangle(x0, y0, x1, y1);
    }

    Canvas() {
        System.out.println("Canvas " + idxCanvas + " was created");
        idxCanvases++;
    }

    public void addShape(Shape s) {
        if (shapes == null) {
            shapes = new Shape[10];
        }
        if (idxShapes < MAX_NO_SHAPES) {
            shapes[idxShapes] = s;
            System.out.println("Shape " + (idxShapes + 1) + " (" + s + ") added to Canvas " + idxCanvas);
            idxShapes++;
        } else {
            System.out.println("Canvas is full. No more shapes can be created");
        }
    }

    public void removeShape() {
         if (idxShapes > 0) {
             System.out.println("Shape " + idxShapes + " (" + shapes[idxShapes - 1] + ") removed from Canvas " + idxCanvas);
             shapes[idxShapes - 1] = null;
             idxShapes--;
        } else {
            System.out.println("Canvas is empty. No more shapes to be removed");
        }
    }

    @Override
    public String toString() {
        return "Canvas " + idxCanvas;
    }
}