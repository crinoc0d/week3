public interface Shape {
    void printArea();
}

class Line {
    private double length;
    Point[] points = new Point[2];

    Line(Point p0, Point p1) {
        this.length = Math.abs(p0.x - p1.x + p0.y - p1.y);
        points[0] = p0;
        points[1] = p1;
    }

    public double getLength() {
        return length;
    }

/*    public Point[] getPoints() {
        return points;
    }*/
}


class Point {
    double x, y;

    Point(double x, double y) {
        this.x = x;
        this.y = y;
    }
}
