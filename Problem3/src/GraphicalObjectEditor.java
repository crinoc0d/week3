public class GraphicalObjectEditor {
    public static void main(String[] args) {
        Panel panel = new Panel();

        Canvas canvas1 = new Canvas();
        Shape shape1 = canvas1.getRectangle(0, 1, 3, 4);
        Shape shape2 = canvas1.getCircle(5);
        Shape shape3 = canvas1.getRectangle(2, 4, 7, 9);
        canvas1.addShape(shape1);
        canvas1.addShape(shape2);
        canvas1.addShape(shape3);
        System.out.println();

        Canvas canvas2 = new Canvas();
        Shape shape4 = canvas2.getCircle(10);
        canvas2.addShape(shape4);
        canvas1.addCanvas(canvas2);
        System.out.println();

        Canvas canvas3 = new Canvas();
        Shape shape5 = canvas3.getRectangle(2, 4, 7, 9);
        Shape shape6 = canvas3.getRectangle(2, 4, 7, 9);
        canvas3.addShape(shape5);
        canvas3.addShape(shape6);
        canvas3.addShape(shape5);
        canvas3.addShape(shape6);
        canvas3.addShape(shape5);
        canvas3.addShape(shape6);
        canvas3.removeShape();
        canvas3.removeShape();
        canvas3.removeShape();
        canvas3.removeShape();
        canvas3.removeShape();
        canvas3.removeShape();
        canvas3.addShape(shape6);
        System.out.println();

        panel.addCanvas(canvas1);
        panel.addCanvas(canvas3);
    }
}
