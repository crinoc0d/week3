public class Panel {
    final int MAX_NO_CANVAS = 10;
    private Canvas[] canvases;
    static int idxCanvases = 0;

    Panel() {
        System.out.println("Panel constructor");
    }

    public void addCanvas(Canvas c) {
        if (canvases == null) {
            canvases = new Canvas[10];
        }
        canvases[idxCanvases] = c;
        System.out.println("Canvas " + c.idxCanvas + " added to " + this.toString());
    }

    public void removeCanvas() {
        if (idxCanvases > 0) {
            System.out.println("Canvas " + canvases[idxCanvases].idxCanvas + " removed from " + this.toString());
            canvases[idxCanvases] = null;
            idxCanvases--;
        } else {
            System.out.println("Panel is empty. No more canvases to be removed");
        }
    }

    @Override
    public String toString() {
        return "Panel";
    }
}
